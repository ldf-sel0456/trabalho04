import sys
import requests

url = 'http://localhost:5000/api'
data = {"fact": int(sys.argv[1]), "fibo": int(sys.argv[2])}

resp = requests.post(url, json=data)
print(resp.text)
