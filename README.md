# Trabalho 04

**Objetivo:** fazer uma API em Flask que retorne o resultado das operações fatorial e/ou Fibonacci com entrada e saída no formato JSON.

Para rodar a API utilize o seguinte comando:
```
flask --app app run
```

Para enviar uma requisição à API utilize:
```
curl http://localhost:5000/api -H 'Content-Type: application/json' -d @data.json
```

Ou use o script `send.py` passando os valores das variáveis `fact` e `fibo` pelo terminal:
```
python3 send.py <fact> <fibo>
```

Por exemplo, para enviar `fact = 5` e `fibo = 10`:
```
python3 send.py 5 10
```
O script `send.py` suporta apenas valores inteiros como argumentos. Para testar outras combinações de requisições, utilizar o primeiro método.
