import json
from flask import Flask, request

def factorial(n):
	a = 1
	for  i in range(1, n + 1):
		a *= i
	return a
	
def fibonacci(n):
	a, b = 0, 1
	for _ in range(n):
		a, b = a + b, a
	return a

def check_input(num, nmin, nmax):
	r = False
	if isinstance(num, int):
		r = (nmin <= num <= nmax)
	return r

app = Flask(__name__)

@app.route('/api', methods=['POST'])
def api():
	if request.is_json:
		data = request.json
		fact = data.get('fact')
		fibo = data.get('fibo')
		
		rfact = None
		if not fact == None:
			if check_input(fact, 0, 100):
				rfact = factorial(fact)
			else:
				return "400 Bad Request: Invalid JSON 'fact' value", 400
				
		rfibo = None
		if not fibo == None:
			if check_input(fibo, 0, 100):
				rfibo = fibonacci(fibo)
			else:
				return "400 Bad Request: Invalid JSON 'fibo' value", 400
				
		r = {'fact': rfact, 'fibo': rfibo}
		return json.dumps(r), 200
		
	else:
		return '400 Bad Request: Incorrect JSON header', 400

# curl http://localhost:5000/api -H 'Content-Type: application/json' -d @data.json
